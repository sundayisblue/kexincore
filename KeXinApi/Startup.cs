using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac;
using BoYuanCore.Framework;
using KeXin.Model.Config;
using KeXinApi.Code;
using KeXinApi.Code.EnumClass;
using KeXinApi.Code.Filter;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;


namespace KeXinApi
{
    public class Startup
    {
        private string basePath => AppContext.BaseDirectory;
        private readonly IWebHostEnvironment _env;
        private readonly ConfigHelper _configHelper;
        private readonly JwtConfig _jwtConfig;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            _env = env;
            Configuration = configuration; 
            _configHelper = new ConfigHelper();
            _jwtConfig = _configHelper.Get<JwtConfig>("JwtConfig", env.EnvironmentName) ?? new JwtConfig();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //更改默认配置的路径
            services.AddSingleton(new Appsettings(Path.Combine(AppContext.BaseDirectory, "configs")));//注意使用的bin目录下configs文件夹
            //services.TryAddSingleton<IHttpContextAccessor,HttpContextAccessor>();
            services.AddHttpContextAccessor();

            //上传文件大小设置
            services.Configure<FormOptions>(option =>
            {
                option.ValueLengthLimit = int.MaxValue;
                option.MultipartBodyLengthLimit = int.MaxValue; //500 * 1024 * 1024;
                option.MultipartHeadersLengthLimit = int.MaxValue;
            });

            //应用配置
            services.AddSingleton(_jwtConfig);
            //注册默认主库(主库含租户数据库信息)
            services.AddSingleton<IFreeSql>(FreesqlDefault.GetDefaultFreeSql());

            #region Cors 跨域
            services.AddCors(options =>
            {
                options.AddPolicy("Limit", policy =>
                {
                    policy
                        //.SetIsOriginAllowedToAllowWildcardSubdomains()//允许用通配符表示跨域设置，比如 http://*:3000 设置所有请求是3000端口ip都可以跨域
                        .WithOrigins(_jwtConfig.CorUrls)
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });

                //允许前端跨域
                /* //参考
                https://www.cnblogs.com/ang/p/13206871.html#_label11
                https://www.cnblogs.com/xishuai/p/aspnet-core-cors.html
                http://config.net.cn/opensource/protof/984e8179-6baf-4a42-b527-57ea68fcb077-p1.html
                https://docs.microsoft.com/zh-cn/aspnet/core/security/cors?view=aspnetcore-6.0
                 */
                options.AddPolicy("CorsPolicy",
                    builder =>
                    {
                        builder
                            .AllowAnyMethod()
                            .SetIsOriginAllowed(_ => true)
                            .AllowAnyHeader()
                            .AllowCredentials()
                            .WithOrigins("http://localhost:20711");
                    });

                /*
                //浏览器会发起2次请求,使用OPTIONS发起预检请求，第二次才是api异步请求
                options.AddPolicy("All", policy =>
                {
                    policy
                    .AllowAnyOrigin()
                    .SetPreflightMaxAge(new TimeSpan(0, 10, 0))
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
                });
                */
            });
            #endregion

            #region Swagger Api文档

            if (_jwtConfig.HaveSwagger)
            {
                services.AddSwaggerGen(options =>
                {
                    typeof(ApiVersion).GetEnumNames().ToList().ForEach(version =>
                    {
                        options.SwaggerDoc(version, new OpenApiInfo
                        {
                            Title = "KeXinApi",
                            Version = version,
                            Description = "小程序api接口"
                        });
                        //c.OrderActionsBy(o => o.RelativePath);
                    });


                    //启用中文注释功能 (以下xml文件需要在项目里设置生成路径) 参考：https://www.cnblogs.com/cxxtreasure/p/14351036.html
                    var xmlPath = Path.Combine(basePath, "KeXinApi.xml");
                    options.IncludeXmlComments(xmlPath, true);
                    //添加对控制器的标签
                    options.DocumentFilter<SwaggerHelper.SwaggerDocTag>();

                    // var xmlEntitiesPath = Path.Combine(basePath, "BSB.Entities.xml");
                    // options.IncludeXmlComments(xmlEntitiesPath);

                    var xmlServicesPath = Path.Combine(basePath, "KeXin.Service.xml");
                    options.IncludeXmlComments(xmlServicesPath);

                    var xmlModelPath = Path.Combine(basePath, "KeXin.Model.xml");
                    options.IncludeXmlComments(xmlModelPath);

                    #region Swagger使用鉴权组件
                    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                    {
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.ApiKey,
                        Description = "直接在下框中输入Bearer {token}（注意Bearer {token}之间是一个空格）",
                        Name = "Authorization",
                        BearerFormat = "JWT",
                        Scheme = "Bearer"
                    });
                    options.AddSecurityRequirement(new OpenApiSecurityRequirement
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference=new OpenApiReference
                                {
                                    Type=ReferenceType.SecurityScheme,
                                    Id="Bearer"
                                }
                            },
                            new string[] {}
                        }
                    });
                    #endregion
                });

                services.AddSwaggerGenNewtonsoftSupport();//swagger使用Newtonsoft(nuget: Swashbuckle.AspNetCore.Newtonsoft)
            }

            #endregion

            #region Jwt 鉴权

            services.AddCustomJWT(_jwtConfig);

            #endregion

            #region 注入BA授权验证
            /*
            //此BA会根据[Authorize]特性影响controller
            //从配置文件里获取用BA的用户名和密码信息
            BasicAuthenticationOption basicOption = new BasicAuthenticationOption();
            Configuration.Bind("Basic", basicOption);

            //https://www.cnblogs.com/JulianHuang/p/10345365.html
            services.AddAuthentication(BasicAuthenticationScheme.DefaultScheme)
                .AddScheme<BasicAuthenticationOption, BasicAuthenticationHandler>(BasicAuthenticationScheme.DefaultScheme, r =>
                {
                    r.UserName = basicOption.UserName;
                    r.UserPwd = basicOption.UserPwd;
                    r.Realm = basicOption.Realm;
                });
            */
            #endregion

            #region 控制器
            services.AddControllers(options =>
                {
                    //全局异常拦截 过滤器方式改成中间件形式 参考:app.ConfigureExceptionHandler()
                    //options.Filters.Add(new ApiExceptionFilter());
                    //去除ActionAsync后缀
                    options.SuppressAsyncSuffixInActionNames = true;
                })
                .AddNewtonsoftJson(options =>
                {
                    //忽略循环引用
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    //使用驼峰 首字母小写
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    //设置时间格式
                    options.SerializerSettings.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.MicrosoftDateFormat;
                    options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                });
            #endregion


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            // 对swagger访问地址进行 BA授权检验。不好的是每个接口需要ba验证。
            //app.UseWhen(
            //    predicate: x => x.Request.Path.StartsWithSegments(new PathString("/index.html"))
            //                    || x.Request.Path.StartsWithSegments(new PathString("/swagger/v1/swagger.json"))
            //    ,
            //    configuration: appBuilder => { appBuilder.UseBasicAuthentication(); }
            //);


            //允许body重复读取(异常时获取请求json)
            app.Use(next => context =>
            {
                context.Request.EnableBuffering();
                return next(context);
            });

            //异常
            //app.UseExceptionHandler("/Error");//mvc跳转错误页面方式,webapi不需要
            app.ConfigureExceptionHandler();//全局异常中间件 

            //静态文件
            app.UseStaticFiles();

            //路由
            app.UseRouting();

            //跨域
            app.UseCors("Limit");

            #region 全局跨域设置origin
            /*
            //https://jasonwatmore.com/post/2021/05/26/net-5-api-allow-cors-requests-from-any-origin-and-with-credentials
            // global cors policy
            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true) // allow any origin
                .AllowCredentials()); // allow credentials
            */
            #endregion

            //认证
            app.UseAuthentication();

            //授权
            app.UseAuthorization();

            //配置端点
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            #region Swagger Api文档
            if (_jwtConfig.HaveSwagger)
            {
                //app.UseSwaggerAuthorized();//swagger首页授权登录
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    typeof(ApiVersion).GetEnumNames().OrderByDescending(e => e).ToList().ForEach(version =>
                    {
                        c.SwaggerEndpoint($"/swagger/{version}/swagger.json", $"KeXinApi {version}");
                    });
                    c.RoutePrefix = "";//直接根目录访问，如果是IIS发布可以注释该语句，并打开launchSettings.launchUrl
                    c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);//折叠Api
                    //c.DefaultModelsExpandDepth(-1);//不显示Models

                    //更改swagger ui 首页为自定义页面 参考: https://mac-blog.org.ua/dotnet-core-swashbuckle-3-bearer-auth/
                    //每次更改index.html 都需要重启项目
                    var assembly = GetType().GetTypeInfo().Assembly;
                    var ns = assembly.GetName().Name;
                    c.IndexStream = () => assembly.GetManifestResourceStream($"{ns}.index.html");
                });
            }
            #endregion 
           
        }

        #region autofac 相关

        public void ConfigureContainer(ContainerBuilder containerBuilder)
        {
            //containerBuilder.RegisterType<TestServiceE>().As<ITestServiceE>().SingleInstance();
            containerBuilder.RegisterModule<AutofacModuleRegister>();
        }

        public class AutofacModuleRegister : Autofac.Module
        {
            //重写Autofac管道Load方法，在这里注册注入
            protected override void Load(ContainerBuilder builder)
            {
                //当前项目引用要注入的dll，比如说BoYuanCore.Services，BoYuanCore.SqlSugarRepositories等，如果没有引用则注入失败！下面代码无执行顺序

                ////注册Service中的对象,Services中的类要以Services结尾，否则注册失败
                //builder.RegisterAssemblyTypes(GetAssemblyByName("BoYuanCore.Services"))
                //    .Where(a => a.Name.EndsWith("Services"))
                //    .AsImplementedInterfaces()
                //    .InstancePerDependency();

                ////注册Repository中的对象,Repository中的类要以Repository结尾，否则注册失败。并注册UnitOfWork对象
                //builder.RegisterAssemblyTypes(GetAssemblyByName("BoYuanCore.SqlSugarRepositories"))
                //    .Where(a => a.Name.EndsWith("Repository") || a.Name == "UnitOfWork")
                //    .AsImplementedInterfaces()
                //    .InstancePerDependency();

                /* //https://www.cnblogs.com/daryl/p/7778190.html
                InstancePerLifetimeScope：同一个Lifetime生成的对象是同一个实例
                SingleInstance：单例模式，每次调用，都会使用同一个实例化的对象；每次都用同一个对象；
                InstancePerDependency：默认模式，每次调用，都会重新实例化对象；每次请求都创建一个新的对象；
                */

                var basePath = AppContext.BaseDirectory;
                var servicesDllFile = Path.Combine(basePath, "KeXin.Service.dll");

                if (!(File.Exists(servicesDllFile)))
                {
                    throw new Exception("service.dll 文件丢失，因为项目解耦了，所以需要先F6编译，再F5运行，请检查 bin 文件夹，并拷贝。");
                }

                //uow
                //builder.RegisterType<FreeSql.UnitOfWorkManager>().InstancePerLifetimeScope(); //Scoped

                // 获取 Service.dll 程序集服务，并注册
                var assemblysServices = Assembly.LoadFrom(servicesDllFile);
                builder.RegisterAssemblyTypes(assemblysServices)
                    .AsImplementedInterfaces()
                    .InstancePerDependency();
                //.EnableInterfaceInterceptors()//引用Autofac.Extras.DynamicProxy;
                //.InterceptedBy(cacheType.ToArray());//允许将拦截器服务的列表分配给注册。

                //注册ILogger
                builder.RegisterGeneric(typeof(Logger<>))
                    .As(typeof(ILogger<>))
                    .SingleInstance();
            }

        }
        /* autofac 注入相关        
            //https://www.cnblogs.com/Vam8023/p/10684651.html 
            //https://www.cnblogs.com/qixuejia/p/5009837.html
            //https://www.jb51.net/article/95763.htm
            //https://www.cnblogs.com/yanweidie/p/autofac.html
         */
        #endregion


    }
}
