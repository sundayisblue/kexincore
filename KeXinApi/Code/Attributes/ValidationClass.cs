﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KeXinApi.Code.Attributes
{
    /// <summary>
    /// 自定义模型验证
    /// </summary>
    public class ValidationClass
    {
        /// <summary>
        /// 第几页验证
        /// </summary>
        public class PageIndexValidateAttribute:ValidationAttribute
        {
            public PageIndexValidateAttribute()
            {

            }

            //public PageIndexValidateAttribute(int pageIndex)
            //{
            //     PageIndex=pageIndex;
            //}
            //public int PageIndex { get; set; }//为IsValid对比值用

            protected override ValidationResult IsValid(object value, ValidationContext context)
            {
                if (value != null && int.TryParse(value.ToString(),out var result) && result>0)
                {
                    return  ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult("第几页只能是大于等于1的整数。");
                }
            }
        }
    }
}
