using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Autofac.Extensions.DependencyInjection;

namespace KeXinApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                // 将配置传给 Serilog 的提供程序 
                //.ReadFrom.Configuration(Configuration)
                .Enrich.With(new DateTimeNowEnricher())
                .MinimumLevel.Debug()//最小记录级别
                .Enrich.FromLogContext()//记录相关上下文信息 
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)//对其他日志进行重写,除此之外,目前框架只有微软自带的日志组件
                //.WriteTo.Console()//输出到控制台 //.WriteTo.File
                .WriteTo.Async(o =>
                {
                    o.Console();
                    o.File("logs/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/log_.log", restrictedToMinimumLevel: LogEventLevel.Error, rollingInterval: RollingInterval.Day); //输出到文件,需要提供输出路径和周期
                })
                .CreateLogger();

            try
            {
                Log.Information("Starting web host");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                //todo 
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        //.UseContentRoot(Directory.GetCurrentDirectory())
                        .UseIISIntegration()
                        .ConfigureAppConfiguration((config) =>
                        {
                            //设置热更新。注意AppContext.BaseDirectory在项目调试里是bin\\configs目录下(而不是项目\\configs下)。改appsettings.json注意位置
                            config.AddJsonFile(Path.Combine(AppContext.BaseDirectory, "configs", "appsettings.json"), true, true);
                        })
                        .UseStartup<Startup>();
                })
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())//Autofac ioc容器
                .UseSerilog(dispose: true); //logger注入使用Serilog日志 dispose 参数设置为 true 会在程序退出时释放日志对象


        #region Serilog 相关设置
        class DateTimeNowEnricher : ILogEventEnricher
        {
            public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
            {
                logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty(
                    "DateTimeNow", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            }
        }

        #endregion
    }
}
