﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using KeXin.IService;
using KeXin.Model.Input;
using KeXin.Model.Output;

namespace KeXinApi.Controllers
{
    public class AdminController : BaseController
    {
        private readonly IAdminService _adminService;

        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponseOutput> GetPageListAsync([FromBody] AdminInput input)
        {
            var list= await _adminService.GetPageListAsync(input);

            return ResponseOutput.Ok(list, "成功");
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponseOutput> GetInfoAsync([Required] long id)
        {
            var list = await _adminService.GetInfoAsync(id);

            return ResponseOutput.Ok(list, "成功");
        }

        /// <summary>
        /// 获取token信息，获取用户id
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponseOutput> GetAdminIdAsync()
        {
            return ResponseOutput.Ok(_adminService.GetAdminIDByToken());
        }

    }
}
