﻿using Microsoft.AspNetCore.Http;

using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BoYuanCore.Framework;
using KeXin.Model.Input;
using KeXin.Model.Output;
using Microsoft.AspNetCore.Hosting;

namespace KeXinApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ZipController : ControllerBase
    {
        private readonly IWebHostEnvironment _iWebHostEnvironment;
        public ZipController(IWebHostEnvironment iWebHostEnvironment)
        {
            _iWebHostEnvironment = iWebHostEnvironment;
        }


        /// <summary>
        /// 测试压缩zip，zip文件在wwwroot文件夹下
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponseOutput> TestZipAsync()
        {
            var divPath = Path.Combine(_iWebHostEnvironment.WebRootPath,  "DownLoad");
            var zipPath = Path.Combine(_iWebHostEnvironment.WebRootPath, "DownLoad", "new.zip");
            ZipHelper.ZipDirectory(divPath, zipPath);

            return ResponseOutput.Ok("执行成功", "成功");
        }
   }
}
