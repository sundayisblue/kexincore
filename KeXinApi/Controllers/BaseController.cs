﻿
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.PeerToPeer;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using KeXinApi.Code.Attributes;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace KeXinApi.Controllers
{

    /// <summary>
    /// 基础控制器
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    [ValidateInput]
    public abstract class BaseController : ControllerBase
    {
       
     
    }
}
