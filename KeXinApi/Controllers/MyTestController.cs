﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KeXin.Model.Input;
using KeXin.Model.Output;
using KeXinApi.Code.Attributes;

namespace KeXinApi.Controllers
{
    /// <summary>
    /// 基础控制器
    /// </summary>
    [ValidateInput]
    [Route("api/[controller]/[action]")]
    public class MyTestController : ControllerBase
    {

        /// <summary>
        /// FromBody 测试
        /// </summary>
        /// <param name="input">复杂类型参数,带list</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponseOutput> GetPageListAsync([FromBody] MyTestInput input)
        {
            //FromBody 一般都是接收json格式的提交。content-type: application/json;
            return ResponseOutput.Ok(input, "成功");
        }
    }
}
