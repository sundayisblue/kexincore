﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoYuanCore.Framework;
using KeXin.Model.Input;
using KeXin.Model.Output;
using KeXinApi.Code.Attributes;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;

namespace KeXinApi.Controllers
{
    public class UploadController : BaseController
    {
        private readonly IWebHostEnvironment _env;

        public UploadController(IWebHostEnvironment env)
        {
            _env = env;
        }

        /// <summary>
        /// 上传base64图片示例
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IResponseOutput> UploadImgBase64Async([FromBody] ImgUploadInput img)
        {
            if (!UpLoadContext.IsBase64(img.ImgBase64))
            {
                return ResponseOutput.NotOk("图片非base64格式，请上传真正图片base64编码");
            }

            var url=  UpLoadContext.UpLoadIMGByBase64(img.ImgBase64, _env.WebRootPath);

            //注意，有些接口需要的去掉base64编码头(例如:data:image/jpg;base64,),所以采用UpLoadContext.GetNoHeaderBase64方法
            //这里返回base64 无头编码值。
            return ResponseOutput.Ok("图片Url是:"+ url, "上传图片成功,id是:"+img.Id);
        }


        /// <summary>
        /// 上传图片示例
        /// </summary>
        /// <param name="fileInput"></param>
        /// <returns></returns>
        [HttpPost]
        [EnableCors("CorsPolicy")]//使用前端跨域方式 前端需要类似设置: xhr.setRequestHeader("Access-Control-Allow-Origin", "*");//需要在IIS里面配置，就可以跨域请求了
        //[Consumes("multipart/form-data")] //如果[FromBody] 需要添加此特性。而[FromForm]会默认为multipart/form-data
        public async Task<IResponseOutput> UploadImageAsync([FromForm]FileUploadInput fileInput)
        {
            using (UpLoadContext context=new UpLoadContext(fileInput.FileObj))
            {
                //注意 webapi默认是没有wwwroot文件夹，需要手动创建此文件夹，要不然上传文件无法保存到服务器
                context.WwwrootPath = _env.WebRootPath;
                //注意，如果上传其他文件请用 context.UpLoadFile(),设置上传文件格式,例如上传rar 7z zip等文件: context.MIME= "rar|7z|zip",其他设置具体看UpLoadContext这个工具类
                if (!context.UpLoadIMG())//上传图片
                {
                    return ResponseOutput.NotOk(context.Error);
                }
                //这里只做个简单展示,返回上传图片或文件的路径
                return ResponseOutput.Ok(context.FileName_url, "上传图片成功,id是:" + fileInput.Id);
            }

        }

        /// <summary>
        /// 上传文件示例
        /// </summary>
        /// <param name="fileInput"></param>
        /// <returns></returns>
        [HttpPost]
        [EnableCors("CorsPolicy")]//使用前端跨域方式 前端需要类似设置: xhr.setRequestHeader("Access-Control-Allow-Origin", "*");//需要在IIS里面配置，就可以跨域请求了
        //[Consumes("multipart/form-data")] //如果[FromBody] 需要添加此特性。而[FromForm]会默认为multipart/form-data
        [DisableRequestSizeLimit]//不限制请求大小
        public async Task<IResponseOutput> UploadFileAsync([FromForm] FileUploadInput fileInput)
        {
            //参考 https://www.cnblogs.com/pangjianxin/p/11136670.html

            using (UpLoadContext context = new UpLoadContext(fileInput.FileObj))
            {
                //注意 webapi默认是没有wwwroot文件夹，需要手动创建此文件夹，要不然上传文件无法保存到服务器
                context.WwwrootPath = _env.WebRootPath;
                context.MaxSize = int.MaxValue;//最大文件限制，int.MaxValue不到2GB
                context.MIME += "|mp4|ts";//增加视频文件
                if (!context.UpLoadFile())//上传文件
                {
                    return ResponseOutput.NotOk(context.Error);
                }
                //这里只做个简单展示,返回上传图片或文件的路径
                return ResponseOutput.Ok(context.FileName_url, "上传文件成功,id是:" + fileInput.Id);
            }

        }

    }
}
