# KeXinCore

#### 介绍
基于.Net5 实现WebApi Jwt项目，使用 Swagger FreeSql SeriLog Autofac框架。主要实现了jwt身份验证和授权，以及刷新token功能，算是一个webapi简单的入门架子。

#### 实现的功能

1. 基于jwt的token身份授权验证
2. 刷新token
3. 获取token信息
4. 上传文件或上传图片base64功能
5. 文件下载示例。文件的绝对路径或以流的形式下载
6. 前端跨域的设置, 在配置文件jwtconfig.json里 corUrls设置跨域ip和端口等。
7. 增加BA标准协议，对swagger首页访问进行BA授权验证。
8. zip压缩例子
9. 全局异常中间件，异常拦截获取body请求json并写入日志里

视频实战 https://www.bilibili.com/video/BV1YT4y1D7Mu?spm_id_from=444.41.0.0

#### 如果觉得对您有所帮助，欢迎您支持下作者

<img src="https://gitee.com/uploads/images/2019/0423/190419_15b3388e_436641.png" width="200" >

<img src="https://gitee.com/uploads/images/2019/0423/190842_1c6bdb00_436641.png" width="200" >