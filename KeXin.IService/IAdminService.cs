﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeXin.Model.DTO;
using KeXin.Model.Input;
using KeXin.Model.Output;

namespace KeXin.IService
{
    public interface IAdminService: IBaseService
    {
        Task<PageOutput<AdminDTO>> GetPageListAsync(AdminInput input);

        Task<AdminOutput> GetInfoAsync(long id);

        long GetAdminIDByToken();
    }
}
