﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeXin.IService
{
    public interface IBaseService
    {
        Task<T> GetModelAsync<T>(IFreeSql fsql,object id) where T : class;
    }
}
