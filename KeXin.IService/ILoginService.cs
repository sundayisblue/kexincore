﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeXin.IService
{
    public interface ILoginService : IBaseService
    {
        Task<(string, long)> LoginAsync(string username, string pwd);
        (string, string) GetAccessTokenAndRefreshToken(string UserRealName, string AdminId);
        string RefreshToken();
    }
}
