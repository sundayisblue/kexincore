﻿using System;
using FreeSql.DataAnnotations;
using Newtonsoft.Json;
using Snowflake.Net;

namespace KeXin.Entity
{
    /// <summary>
    /// 雪花id实体
    /// </summary>
    [Serializable]
    public class SnowflakEntity
    {
        /// <summary>
        /// 雪花主键
        /// </summary>
        [JsonProperty]
        [Column(IsPrimary = true)]
        public long ID { get; set; } = IdWorkerHelper.GenId64();//默认雪花id赋值

        // /// <summary>
        // ///     给grid绑定用(由于前端js对long长字符串支持很差，所以改成string类型)
        // /// </summary>
        // [JsonProperty]
        // [Column(IsIgnore = true)]
        // public string GridID => ID.ToString();

        /// <summary>
        /// 默认时区(具体看SnowflakEntity里的注释)
        /// </summary>
        public const DateTimeKind DefaultDateTimeKind = DateTimeKind.Local;//默认为本地时区。DateTimeKind.Utc 为utc时间
        /*
        只要是DateTimeKind.Local ，insert udpate语句默认会插入getDate(); 无参数，使用sql方法
        只要是DateTimeKind.Utc ，insert udpate语句默认会插入getutcDate(); 无参数，使用sql方法
        如果想update 改写成自己的时间，请使用 .Set(a => a.Update_time, xxx) 
         */
    }
}