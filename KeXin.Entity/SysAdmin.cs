﻿using System;
using Newtonsoft.Json;
using FreeSql.DataAnnotations;
namespace KeXin.Entity 
{
    /// <summary>
    /// 后台用户
    /// </summary>
	[JsonObject(MemberSerialization.OptIn)]
    [Table(Name = "SysAdmin")]
    [Serializable]
    public partial class SysAdmin:SnowflakEntity 
	{
        public SysAdmin()
        {
            this.Isdel = false;
            this.LoginName = string.Empty;
            this.Password = string.Empty;
            this.Photo = string.Empty;
            this.RealName = string.Empty;
            this.Remark = string.Empty;
            this.RoleID = 0;
            this.Sort = 1000;
        }

        /// <summary>
		/// 是否假删除
		/// </summary>
		[JsonProperty]
		public bool? Isdel { get; set; }

		/// <summary>
		/// 登录名称
		/// </summary>
		[JsonProperty, Column(StringLength = 25)]
		public string LoginName { get; set; }

		/// <summary>
		/// 密码
		/// </summary>
		[JsonProperty, Column(StringLength=32)]
		public string Password { get; set; } 

		/// <summary>
		/// 照片
		/// </summary>
		[JsonProperty, Column(StringLength = 100)]
		public string Photo { get; set; } 

		/// <summary>
		/// 真实姓名
		/// </summary>
		[JsonProperty, Column(StringLength = 50)]
		public string RealName { get; set; } 

		/// <summary>
		/// 备注
		/// </summary>
		[JsonProperty, Column(StringLength = 500)]
		public string Remark { get; set; } 

		/// <summary>
		/// 权限角色
		/// </summary>
		[JsonProperty]
		public long RoleID { get; set; }

		/// <summary>
		/// 排序
		/// </summary>
		[JsonProperty]
		public int? Sort { get; set; }

	}

}
