﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeSql.DataAnnotations;
using Snowflake.Net;

namespace FreeSqlExtensions
{
    /// <summary>
    /// 雪花id实体
    /// </summary>
    [Serializable]
    public class SnowflakEntity
    {
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        [Column(IsPrimary = true)]

        public virtual long ID { get; set; } = IdWorkerHelper.GenId64();//默认雪花id赋值

        /// <summary>
        /// 给grid绑定用(由于前端js对long长字符串支持很差，所以改成string类型)
        /// </summary>
        [Column(IsIgnore = true)]
        public virtual string GridID => ID.ToString();


        /// <summary>
        /// 默认时区(具体看SnowflakEntity里的注释)
        /// </summary>
        public const DateTimeKind DefaultDateTimeKind = DateTimeKind.Local;//默认为本地时区。DateTimeKind.Utc 为utc时间
        /*
        只要是DateTimeKind.Local ，insert udpate语句默认会插入getDate(); 无参数，使用sql方法
        只要是DateTimeKind.Utc ，insert udpate语句默认会插入getutcDate(); 无参数，使用sql方法
        如果想update 改写成自己的时间，请使用 .Set(a => a.Update_time, xxx) 
         */
    }
}
