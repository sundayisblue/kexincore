﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeXin.IService;

namespace KeXin.Service
{
    public class BaseService:IBaseService
    {
        public BaseService()
        {

        }

        public async Task<T> GetModelAsync<T>(IFreeSql fsql, object id) where T : class
        {
            return await fsql.Select<T>().WhereDynamic(id).FirstAsync();
        }
    }
}
