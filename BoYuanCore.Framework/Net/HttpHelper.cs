﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;
using BoYuanCore.Framework.Extensions;
using Microsoft.AspNetCore.Http;

namespace BoYuanCore.Framework.Net
{
    /*
     注意！https://www.cnblogs.com/spilledlight/articles/10710848.html
     HttpContextAccessor context = new HttpContextAccessor();
     需要 startup.cs 的 ConfigureServices 中注入 services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
     */


    /// <summary>
    /// http相关工具类
    /// </summary>
    public class HttpHelper
    {
        /// <summary>
        /// get请求方法
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetRemotePage(string url)
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            try
            {
                request = System.Net.WebRequest.Create(url) as HttpWebRequest;
                request.Method = "GET";
                response = request.GetResponse() as HttpWebResponse;
            }
            catch (WebException exception)
            {
                if (exception.Status == WebExceptionStatus.ProtocolError)
                {
                    response = (HttpWebResponse)exception.Response;
                }
                if (exception.Status == WebExceptionStatus.ConnectFailure)
                {
                    return exception.Message;
                }
            }
            catch (Exception)
            {
                if (response != null)
                {
                    response.Close();
                }
                return string.Empty;
            }

            if (response != null)
            {
                Encoding enc = string.IsNullOrEmpty(response.CharacterSet) ? Encoding.UTF8 : Encoding.GetEncoding(response.CharacterSet);
                using (StreamReader sr = new StreamReader(response.GetResponseStream(), enc))
                {
                    string tempStr = sr.ReadToEnd();
                    response.Close();
                    return tempStr;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// post提交,获取返回参数
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="value">参数(username=a&pwd=b)</param>
        /// <returns></returns>
        public static string PostRemotePage(string url, string value)
        {
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            try
            {
                request = (HttpWebRequest)System.Net.WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                byte[] bytes = Encoding.UTF8.GetBytes(value);
                request.ContentLength = bytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException exception)
            {
                if (exception.Status == WebExceptionStatus.ProtocolError)
                {
                    response = (HttpWebResponse)exception.Response;
                }
                if (exception.Status == WebExceptionStatus.ConnectFailure)
                {
                    return exception.Message;
                }
            }
            catch (Exception)
            {
                if (response != null)
                {
                    response.Close();
                }
                return string.Empty;
            }

            if (response != null)
            {
                Encoding enc = string.IsNullOrEmpty(response.CharacterSet) ? Encoding.UTF8 : Encoding.GetEncoding(response.CharacterSet);
                using (StreamReader sr = new StreamReader(response.GetResponseStream(), enc))
                {
                    string tempStr = sr.ReadToEnd();
                    response.Close();
                    return tempStr;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// 添加Host或Connection 的value值 (header里有些不太好添加的值，用此方法)
        /// </summary>
        /// <param name="header"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public static void SetHeaderValue(WebHeaderCollection header, string name, string value)
        {
            var property = typeof(WebHeaderCollection).GetProperty("InnerCollection",
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            if (property != null)
            {
                var collection = property.GetValue(header, null) as NameValueCollection;
                collection[name] = value;
            }
        }

        #region 爬虫请求写法示例
        /// <summary>
        /// 获取百度site:域名 爬虫示例，包含header cookie gzip解压缩
        /// </summary>
        /// <param name="url">例如 https://www.baidu.com/s?wd=site%3Awww.123456.com </param>
        /// <returns></returns>
        private string GetBaiduPage(string url)
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            try
            {
                request = System.Net.WebRequest.Create(url) as HttpWebRequest;
                request.Method = "GET";
                request.Host = "www.baidu.com";

                request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36";
                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3";

                //request.Connection = "keep-alive";
                //正常来说添加Connection值会报错，而且非必填项。但如果一定要给Connection添加值，用下面的方法。
                //SetHeaderValue(request.Headers, "Connection", "keep-alive");

                //如果有gzip压缩，自动解压缩
                request.AutomaticDecompression = DecompressionMethods.GZip;//自动解压缩 （gzip, deflate）

                //增加其他header属性
                request.Headers.Add("cache-control", "max-age=0");
                request.Headers.Add("Upgrade-Insecure-Requests", "1");
                request.Headers.Add("Sec-Fetch-User", "?1");
                request.Headers.Add("Sec-Fetch-Site", "none");
                request.Headers.Add("Sec-Fetch-Mode", "navigate");
                request.Headers.Add("Accept-Encoding", "gzip, deflate, br");
                request.Headers.Add("Accept-Language", "zh-CN,zh;q=0.9");

                //增加cookie值
                CookieCollection cookies = new CookieCollection();
                cookies.Add(new Cookie("BIDUPSID", "D80550B5F377F705B0387391E916CD9C"));
                cookies.Add(new Cookie("PSTM", "1571015334"));
                cookies.Add(new Cookie("BD_UPN", "12314753"));
                cookies.Add(new Cookie("BAIDUID", "7E289D5244475837F98CC5E2D641E014:FG"));
                cookies.Add(new Cookie("BDORZ", "B490B5EBF6F3CD402E515D22BCDA1598"));
                cookies.Add(new Cookie("MCITY", "-58%3A"));
                cookies.Add(new Cookie("BDUSS", "3c1S0xtQWd1bElVSGlxdS1sVTk0d1NucGRsd0wwT1Y2ZzV5WGlLc3dDNmZzZzVlRVFBQUFBJCQAAAAAAAAAAAEAAAClfvUDenkzMjAwMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJ8l512fJeddQ"));
                cookies.Add(new Cookie("delPer", "0"));
                cookies.Add(new Cookie("BD_CK_SAM", "1"));
                cookies.Add(new Cookie("PSINO", "2"));
                cookies.Add(new Cookie("BD_HOME", "1"));
                cookies.Add(new Cookie("BDRCVFR[feWj1Vr5u3D]", "I67x6TjHwwYf0"));
                cookies.Add(new Cookie("H_PS_PSSID", "1423_21111_30210_20697_22158"));
                cookies.Add(new Cookie("COOKIE_SESSION", "5087_0_9_9_2_19_0_4_9_7_1_0_0_0_0_0_1575611490_0_1575617174%7C9%2317077_124_1575524447%7C9"));
                cookies.Add(new Cookie("sug", "3"));
                cookies.Add(new Cookie("sugstore", "1"));
                cookies.Add(new Cookie("ORIGIN", "2"));
                cookies.Add(new Cookie("bdime", "0"));
                cookies.Add(new Cookie("H_PS_645EC", "152cjmp168G1Y96YFvAzbyqeUnjPlhXlOYhjAxwMcI%2FrWppEwXzkPfYu%2Fto"));

                CookieContainer co = new CookieContainer();
                co.Add(new Uri(url), cookies);
                request.CookieContainer = co;

                response = request.GetResponse() as HttpWebResponse;
            }
            catch (WebException exception)
            {
                if (exception.Status == WebExceptionStatus.ProtocolError)
                {
                    response = (HttpWebResponse)exception.Response;
                }
                if (exception.Status == WebExceptionStatus.ConnectFailure)
                {
                    return exception.Message;
                }
            }
            catch (Exception ex)
            {
                if (response != null)
                {
                    response.Close();
                }
                return string.Empty;
            }

            if (response != null)
            {
                Encoding enc = string.IsNullOrEmpty(response.CharacterSet) ? Encoding.UTF8 : Encoding.GetEncoding(response.CharacterSet);
                //string html = Gzip.ReadGzip(response.GetResponseStream(), enc); //gzip 解压缩 或设置 request.AutomaticDecompression
                string html = string.Empty;
                using (StreamReader sr = new StreamReader(response.GetResponseStream(), enc))
                { html = sr.ReadToEnd(); }

                response.Close();
                return html;
            }

            return string.Empty;
        }

        #endregion

        #region URL参数加密解密

        /// <summary>
        /// Base64 转 string
        /// </summary>
        /// <param name="base64"></param>
        /// <returns></returns>
        private static string Base64ToString(string base64)
        {
            try
            {
                char[] charBuffer = base64.ToCharArray();
                byte[] bytes = Convert.FromBase64CharArray(charBuffer, 0, charBuffer.Length);
                return Encoding.UTF8.GetString(bytes);
            }
            catch
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// string 转 base64
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string StringToBase64(string value)
        {
            try
            {
                byte[] bytValue = System.Text.Encoding.UTF8.GetBytes(value);

                return Convert.ToBase64String(bytValue);
            }
            catch
            {
                return string.Empty;
            }
        }



        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Url64Encode(string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;
            string s = StringToBase64(StringToBase64(value));

            return s.TrimEnd(new char[] { '=' });

        }

        public static string Url64Encode(int value)
        {
            return Url64Encode(value.ToString());
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Url64Decode(string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;

            string s = string.Empty;

            int nC = value.Length % 4;
            if (nC == 0)
            {
                s = value;
            }
            else
            {
                s = value + Repeat("=", 4 - nC);
            }
            return Base64ToString(Base64ToString(s));
        }
        /// <summary>
        /// 将字符串重复number次
        /// </summary>
        /// <param name="source"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        private static string Repeat(string source, int number)
        {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            for (i = 0; i < number; i++) sb.Append(source);
            return sb.ToString();
        }
        #endregion

        /// <summary>
        /// 取得客户真实IP(如果有代理，会返回多个ip地址)
        /// </summary>
        /// <returns></returns>
        public static string GetTrueUserIP()
        {
            HttpContextAccessor context = new HttpContextAccessor();
            string UserIP = context.HttpContext.Request.Headers["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(UserIP))
                UserIP = context.HttpContext.Request.Headers["REMOTE_ADDR"];
            if (string.IsNullOrEmpty(UserIP))
                UserIP = context.HttpContext.Connection.RemoteIpAddress.ToString();
            if (UserIP == "::1")
                UserIP = "127.0.0.1";
            if (UserIP.IndexOf(".") == -1)//没有“.”肯定是非IPv4格式
                UserIP = "0.0.0.1";
            else
            {
                string temp = UserIP;
                UserIP = string.Empty;

                IPAddress ip;
                if (temp.IndexOf(",") != -1)
                {
                    //有“,”，估计多个代理。
                    temp = temp.Replace(" ", "").Replace("'", "");
                    string[] TempIPs = temp.Split(',');

                    StringBuilder sb = new StringBuilder();
                    foreach (var item in TempIPs)
                    {
                        if (IPAddress.TryParse(item, out ip))
                        {
                            sb.AppendFormat(",{0}", ip.ToString());
                            //UserIP += "," + ip.ToString();
                        }
                    }
                    if (sb.ToString().Length > 0)
                        UserIP = sb.ToString().Substring(1);
                }
                else
                {
                    if (IPAddress.TryParse(temp, out ip))
                        UserIP = ip.ToString();
                }

                if (UserIP.Length == 0)
                    UserIP = "0.0.0.2";
            }
            return UserIP;
            //https://www.cnblogs.com/a14907/p/6445431.html
            /*
             小结：
1、REMOTE_ADDR 不可被修改，但是可能会获得代理服务器的IP，而不是实际客户端的IP。
2、通过 HTTP_VIA、HTTP_X_FORWARDED_FOR 我们可以获得代理服务器所代理的信息，但是这依靠代理服务器的支持。另外，这两个值可以被修改。我们通过它获得的信息可能是不真实的。
另，HTTP_X_FORWARDED_FOR 的信息可能是一个集合，不含 REMOTE_ADDR 中的代理服务器IP。没有一个完美的解决获得客户端IP地址的方法，我们只能在上面2个信息中取舍。
             */
        }


        /// <summary>
        /// 获取用户操作系统信息
        /// </summary>
        /// <returns></returns>
        public static string GetUserOS()
        {
            HttpContextAccessor context = new HttpContextAccessor();
            string strAgentInfo = context.HttpContext.Connection.RemoteIpAddress.ToString();

            if (strAgentInfo.ContainsByIgnoreCase("NT 10") || strAgentInfo.ContainsByIgnoreCase("NT 6.4"))
            {
                return "Windows 10";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("NT 6.3"))
            {
                return "Windows 8.1";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("NT 6.2"))
            {
                return "Windows 8";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("NT 6.1"))
            {
                return "Windows 7";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("NT 6.0"))
            {
                return "Windows Vista/Server 2008";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("NT 5.2"))
            {
                return "Windows 2003";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("NT 5.1"))
            {
                return "Windows XP";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("NT 5"))
            {
                return "Windows 2000";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("NT 4.9"))
            {
                return "Windows ME";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("NT 4"))
            {
                return "Windows NT4";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("NT 98"))
            {
                return "Windows 98";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("NT 95"))
            {
                return "Windows 95";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("Mac"))
            {
                return "Mac";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("unix"))
            {
                return "UNIX";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("linux"))
            {
                return "Linux";
            }
            else if (strAgentInfo.ContainsByIgnoreCase("SunOS"))
            {
                return "SunOS";
            }

            return "其他系统";
        }

        /// <summary>
        /// 判断是否为手机端
        /// </summary>
        /// <returns></returns>
        public static bool IsMoblie()
        {
            HttpContextAccessor context = new HttpContextAccessor();
            string agent = (context.HttpContext.Connection.RemoteIpAddress.ToString() + "").Trim().ToLower();

            if (agent == "" ||
                agent.IndexOf("mobile") != -1 ||
                agent.IndexOf("mobi") != -1 ||
                agent.IndexOf("nokia") != -1 ||
                agent.IndexOf("samsung") != -1 ||
                agent.IndexOf("sonyericsson") != -1 ||
                agent.IndexOf("mot") != -1 ||
                agent.IndexOf("blackberry") != -1 ||
                agent.IndexOf("lg") != -1 ||
                agent.IndexOf("htc") != -1 ||
                agent.IndexOf("j2me") != -1 ||
                agent.IndexOf("ucweb") != -1 ||
                agent.IndexOf("opera mini") != -1 ||
                agent.IndexOf("mobi") != -1 ||
                agent.IndexOf("android") != -1 ||
                agent.IndexOf("iphone") != -1)
            {
                //终端可能是手机
                return true;
            }

            return false;
        }

        /// <summary>
        /// 判断是否是微信客户端
        /// </summary>
        /// <returns></returns>
        public static bool IsWechat()
        {
            HttpContextAccessor context = new HttpContextAccessor();
            string param = context.HttpContext.Request.Headers["HTTP_USER_AGENT"];
            if (!string.IsNullOrEmpty(param))
            {
                return param.ToLower().Contains("MicroMessenger".ToLower());
            }
            return false;
        }


        #region Gzip 压缩解压缩
        public class Gzip
        {
            /// <summary>
            /// 将Gzip的byte数组读取为字符串
            /// </summary>
            /// <param name="bytes"></param>
            /// <param name="ecd">编码规则</param>
            /// <returns></returns>
            public static string ReadGzip(byte[] bytes, Encoding ecd)
            {
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    return ReadGzip(ms, ecd);
                }
            }

            /// <summary>
            /// 将Gzip的byte数组读取为字符串
            /// </summary>
            /// <param name="ms">流</param>
            /// <param name="ecd">编码</param>
            /// <returns></returns>
            public static string ReadGzip(Stream ms, Encoding ecd)
            {
                using (GZipStream decompressedStream = new GZipStream(ms, CompressionMode.Decompress))
                {
                    using (StreamReader sr = new StreamReader(decompressedStream, ecd))
                    {
                        return sr.ReadToEnd();
                    }
                }
            }

            /// <summary>
            /// 将字符串压缩成Gzip格式的byte数组
            /// </summary>
            /// <param name="str"></param>
            /// <returns></returns>
            public static byte[] WriteGzip(string str)
            {
                byte[] rawData = System.Text.Encoding.UTF8.GetBytes(str);
                using (MemoryStream ms = new MemoryStream())
                {
                    GZipStream compressedzipStream = new GZipStream(ms, CompressionMode.Compress, true);
                    compressedzipStream.Write(rawData, 0, rawData.Length);
                    compressedzipStream.Close();
                    return ms.ToArray();
                }
            }
        }
        #endregion

    }
}
