﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BoYuanCore.Framework.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// 忽略大小写 是否包含指定的字符串
        /// </summary>
        /// <param name="source"></param>
        /// <param name="toCheck"></param>
        /// <returns></returns>
        public static bool ContainsByIgnoreCase(this string source, string toCheck)
        {
            return source.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        /// <summary>
        /// 忽略大小写 是否包含指定的字符串
        /// </summary>
        /// <param name="sourceList"></param>
        /// <param name="toCheck"></param>
        /// <param name="comp">StringComparison.InvariantCultureIgnoreCase</param>
        /// <returns></returns>
        public static bool Contains(this IEnumerable<string> sourceList, string toCheck, StringComparison comp)
        {
            return sourceList.Any(p => ContainsByIgnoreCase((string)p, toCheck));
        }

        /// <summary>
        /// 从头开始截取最大长度的字符串
        /// </summary>
        /// <param name="source"></param>
        /// <param name="maxLength">长度(大于0的长度)</param>
        /// <exception cref="Exception"></exception>
        /// <returns></returns>
        public static string ToMaxLengthString(this string source, int maxLength)
        {
            if (maxLength < 1) throw new Exception("ToMaxLengthString方法中maxLength值必须大于0");
            return source.Length > maxLength ? source.Substring(0, maxLength) : source;
        }

        /// <summary>
        /// 修改第一个字符为大写
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToUpperInitial(this string source)
        {
            if (string.IsNullOrEmpty(source)) return source;
            return source.Substring(0, 1).ToUpper() + source.Substring(1);
        }
        /// <summary>
        /// 修改第一个字符为小写
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToLowerInitial(this string source)
        {
            if (string.IsNullOrEmpty(source)) return source;
            return source.Substring(0, 1).ToLower() + source.Substring(1);
        }

        /// <summary>
        /// 按字符串(或表达式)分隔字符串数组
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="separatorString">分隔符:字符串(或表达式)</param>
        /// <param name="regexOptions">忽略大小写选项等</param>
        /// <returns></returns>
        public static string[] SplitByString(this string input, string separatorString, RegexOptions regexOptions = RegexOptions.IgnoreCase)
        {
            return Regex.Split(input, separatorString, regexOptions);
        }

        /// <summary>
        /// 忽略大小写进行替换字符串
        /// </summary>
        /// <param name="input">输入字符串</param>
        /// <param name="pattern">要替换的样式字符串</param>
        /// <param name="replacement">替换后的字符串</param>
        /// <returns></returns>
        public static string ReplaceStringIgnoreCase(this string input, string pattern, string replacement)
        {
            return Regex.Replace(input, pattern, replacement, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// 换行符(老记不住,故封装一个)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string LineBreak(this string input)
        {
            return input+System.Environment.NewLine;
        }

        /// <summary>
        /// 生成自定义异常消息
        /// </summary>
        /// <param name="ex">异常对象</param>
        /// <param name="exceptionName">备用异常消息</param>
        /// <param name="json">请求json内容</param>
        /// <returns>异常字符串文本</returns>
        public static string GetExceptionMsg(this Exception ex, string exceptionName = "")
        {
            string timeStr = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss fff");
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("");
            sb.AppendLine("****************************异常开始:" + timeStr + "****************************");
            if (!string.IsNullOrWhiteSpace(exceptionName)) sb.AppendLine(exceptionName);
            sb.AppendLine("【异常类型】：" + ex.GetType().Name);
            sb.AppendLine("【异常信息】：" + ex.Message);
            sb.AppendLine("【堆栈调用】：" + ex.StackTrace);
            sb.AppendLine("****************************异常结束:" + timeStr + "****************************");
            return sb.ToString();
        }
    }
}
