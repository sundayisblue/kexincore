﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoYuanCore.Framework.MemoryCache
{
    //ASP.NET Core-内存缓存与分布式缓存（IMemoryCache、IDistributedCache）
    //参考 https://www.cnblogs.com/fanfan-90/p/12151924.html

    /// <summary>
    /// 通用的缓存接口
    /// </summary>
    public interface ICachingProvider
    {
        /// <summary>
        /// 读取或创建缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheItemName"></param>
        /// <param name="objectSettingFunction"></param>
        /// <param name="cacheTimeInMinutes">缓存时间(分钟)</param>
        /// <returns></returns>
        T GetOrSetObjectFromCache<T>(string cacheItemName, Func<T> objectSettingFunction, int cacheTimeInMinutes = 60);

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="cacheTimeInMinutes"></param>
        void SetValueToCache(string key, object value, int cacheTimeInMinutes = 120);

        /// <summary>
        /// 删除一个缓存
        /// </summary>
        /// <param name="key"></param>
        void Remove(string key);

        /// <summary>
        /// 删除所有缓存
        /// </summary>
        void RemoveAll();

        /// <summary>
        /// 从缓存中获取一个值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        object GetValueFromCache(string key);
    }
}
/*

services.AddMemoryCache();//MemoryCache
services.AddSingleton(typeof(ICachingProvider), typeof(MemoryCaching));

//以下是redis
 services.AddDistributedRedisCache(options =>
            {
                ConfigurationOptions configurationOptions = new ConfigurationOptions
                {
                    ConnectTimeout = 2000,
                    DefaultDatabase = 1,                    
                    Password = "315360007",
                    AllowAdmin = true,
                    AbortOnConnectFail = false//当为true时，当没有可用的服务器时则不会创建一个连接
                };
                configurationOptions.EndPoints.Add("192.168.0.82:16379");
                options.ConfigurationOptions = configurationOptions;
            });
//services.AddSingleton(typeof(ICachingProvider), typeof(MemoryCaching));

 */
/*
 //调用示例
            var smsCode= _iCachingProvider.GetValueFromCache(key);
            if (smsCode != null)//判断是否有缓存
            {
                return ResponseOutput.NotOk("请稍后再获取请求");
            }

 var sms=  _iCachingProvider.GetOrSetObjectFromCache(key, ()=> new SMSDTO() { Tel = tel, Code = code, AddTime = DateTime.Now }, 5);
 */
