﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoYuanCore.Framework.StringUtility
{
    public class TimeHelper
    {
        /// <summary>
        /// 和当前时间比对，获取过去的时间内容
        /// </summary>
        /// <param name="beginTime">要对比的时间</param>
        /// <returns></returns>
        public static string GetPastTimeText(DateTime beginTime)
        {
            //如果刷新的时间大于当前时间，说明是提前刷新。算是一天前
            if (beginTime > DateTime.Now) beginTime = beginTime.AddDays(-1);

            return GetPastTimeText(beginTime, DateTime.Now);
        }

        /// <summary>
        /// 开始和结束时间对比，获取过去的时间内容
        /// </summary>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束</param>
        /// <returns></returns>
        public static string GetPastTimeText(DateTime beginTime, DateTime endTime)
        {
            TimeSpan ts = endTime - beginTime;

            return GetPastTimeText(ts);
        }

        /// <summary>
        /// 获取过去的时间内容
        /// </summary>
        /// <param name="ts">时间差</param>
        /// <returns></returns>
        public static string GetPastTimeText(TimeSpan ts)
        {
            if (ts.TotalDays > 365)
            {
                return Math.Round(ts.TotalDays / 365) + "年前";
            }
            else if (ts.TotalDays > 30)
            {
                return Math.Round(ts.TotalDays / 30) + "月前";
            }
            else if (ts.TotalDays > 1)
            {
                return Math.Round(ts.TotalDays) + "天前";
            }
            else if (ts.TotalHours > 1)
            {
                return Math.Round(ts.TotalHours) + "小时前";
            }
            else if (ts.TotalMinutes > 1)
            {
                return Math.Round(ts.TotalMinutes) + "分钟前";
            }
            else
            {
                //return Math.Round(ts.TotalSeconds) + "秒前";
                return "刚刚发表";
            }
        }


        /// <summary>
        /// 获取季度，根据时间
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static int GetQuarterByDateTime(DateTime dateTime)
        {
            return GetQuarterByMonth(dateTime.Month);
        }

        /// <summary>
        /// 获取季度，根据月份
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        public static int GetQuarterByMonth(int month)
        {
            return month / 3 + (month % 3 > 0 ? 1 : 0);
        }

        #region 时间戳

        /// <summary>
        /// 时间戳计时开始时间
        /// </summary>
        private static readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// DateTime转换为10位时间戳（单位：秒）
        /// </summary>
        /// <param name="dateTime"> DateTime</param>
        /// <returns>10位时间戳（单位：秒）</returns>
        public static long DateTimeToTimeStamp(DateTime dateTime)
        {
            return (long)(dateTime.ToUniversalTime() - Jan1st1970).TotalSeconds;
        }

        /// <summary>
        /// DateTime转换为13位时间戳（单位：毫秒）
        /// </summary>
        /// <param name="dateTime"> DateTime</param>
        /// <returns>13位时间戳（单位：毫秒）</returns>
        public static long DateTimeToLongTimeStamp(DateTime dateTime)
        {
            return (long)(dateTime.ToUniversalTime() - Jan1st1970).TotalMilliseconds;
        }

        /// <summary>
        /// 10位时间戳（单位：秒）转换为DateTime
        /// </summary>
        /// <param name="timeStamp">10位时间戳（单位：秒）</param>
        /// <returns>DateTime</returns>
        public static DateTime TimeStampToDateTime(long timeStamp)
        {
            return Jan1st1970.AddSeconds(timeStamp).ToLocalTime();
        }

        /// <summary>
        /// 13位时间戳（单位：毫秒）转换为DateTime
        /// </summary>
        /// <param name="longTimeStamp">13位时间戳（单位：毫秒）</param>
        /// <returns>DateTime</returns>
        public static DateTime LongTimeStampToDateTime(long longTimeStamp)
        {
            return Jan1st1970.AddMilliseconds(longTimeStamp).ToLocalTime();
        }

        #endregion
    }
}
