﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace KeXin.Model.Input
{
    /// <summary>
    /// 上传base64图片
    /// </summary>
    [Serializable]
    public class ImgUploadInput
    {
        /// <summary>
        /// 图片base64编码
        /// </summary>
        public string ImgBase64 { get; set; }

        /// <summary>
        /// 示例其他参数，比如主键
        /// </summary>
        public long Id { get; set; }
    }

    /// <summary>
    /// 流的形式上传文件
    /// </summary>
    [Serializable]
    public class FileUploadInput
    {

        /// <summary>
        /// 示例其他参数，比如主键
        /// </summary>
        public long Id { get; set; }


        /// <summary>
        /// 上传文件
        /// </summary>
        public IFormFile FileObj { get; set; }
    }
}
