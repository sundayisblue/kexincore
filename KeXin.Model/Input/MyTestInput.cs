﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeXin.Model.DTO;

namespace KeXin.Model.Input
{
    public class MyTestInput
    {
        [Required]
        public string Title { get; set; }

        public List<AdminDTO> AdminList { get; set; }
     }

   
}
