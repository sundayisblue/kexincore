﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeXin.Model.Input
{
    [Serializable]
    public class AdminInput : BasePageInput
    {
        public string RealName { get; set; }

        public string LoginName { get; set; }
    }
}
