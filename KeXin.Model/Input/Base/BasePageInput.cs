﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeXin.Model.Input
{

    /// <summary>
    /// 分页信息输入
    /// </summary>
    public class BasePageInput
    {
        /// <summary>
        /// 当前页标
        /// </summary>
        [Required, Range(1, int.MaxValue, ErrorMessage = "只能是大于1的整数")]
        public int PageIndex { get; set; } = 1;

        /// <summary>
        /// 每页大小
        /// </summary>
        [Required, Range(1, 100, ErrorMessage = "只能是1到100的整数")]
        public int PageSize { set; get; } = 20;


    }
}
