﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeXin.Model.Config
{
    /// <summary>
    /// 自定义Claim名称
    /// </summary>
    public class ClaimName
    {
        public const string AdminId = "AdminId";
        //public const string TenantName = "TenantName";
        public const string UserRealName = "UserRealName";
    }
}
