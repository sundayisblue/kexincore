﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeXin.Model.Config
{
    /// <summary>
    /// Jwt配置
    /// </summary>
    public class JwtConfig
    {

        /// <summary>
        /// 发行者
        /// </summary>
        public string Issuer { get; set; } = "http://127.0.0.1:8888";

        /// <summary>
        /// 订阅者
        /// </summary>
        public string Audience { get; set; } = "http://127.0.0.1:8888";

        /// <summary>
        /// 跨域地址，默认 http://*:9000
        /// </summary>
        public string[] CorUrls { get; set; } = new[] {"http://*:20711"};  // = new[]{ "http://*:9000" };

        /// <summary>
        /// 密钥
        /// </summary>
        public string SecurityKey { get; set; } = "zy5211314zbyzkxlyj@forever.net";

        /// <summary>
        /// 刷新token 密钥
        /// </summary>
        public string RefreshSecurityKey { get; set; } = "zy521131Ref345esh4zbyzkxlyj@forever.net";

        /// <summary>
        /// 有效期(分钟)
        /// </summary>
        public int Expires { get; set; } = 120;

        /// <summary>
        /// 刷新有效期(分钟)
        /// </summary>
        public int RefreshExpires { get; set; } = 480;

        /// <summary>
        /// 是否启用Swagger文档
        /// </summary>
        public bool HaveSwagger { get; set; } = false;
    }
}
