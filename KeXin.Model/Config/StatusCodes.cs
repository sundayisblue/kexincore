﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeXin.Model.Config
{
    /// <summary>
    /// 状态码枚举
    /// </summary>
    public enum StatusCodes
    {
        操作成功 = 0,

        操作失败 = 1,

        未登录 = 401,
        权限不足 = 403,
        资源不存在 = 404,

        /// <summary>
        /// 系统内部错误（非业务代码里显式抛出的异常，例如由于数据不正确导致空指针异常、数据库异常等等）
        /// </summary>
        系统内部错误 = 500
    }
}
